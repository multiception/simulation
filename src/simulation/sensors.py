#!/usr/bin/env python

import rospy
import numpy as np
import tf_conversions
from multiception.msg import Polygons
from nav_msgs.msg import Odometry
from geometry_msgs.msg import PoseWithCovarianceStamped, Polygon, Point32
from can_zoe_msgs.msg import Kinematics
from mobileye.msg import Obstacle, ListObstacles, TrafficSign, ListSigns, TrafficLight, ListLights

from . import objects
from . import shapes
from . import _tools
from ._ros_bridge import Publishable, Transformable, Displayable, color_from_hex

class Sensor(Transformable, Publishable):
	def __init__(self, parent, name, transfo, rate, topic_pub, type_pub, noise=[]):
		Transformable.__init__(self, parent, name, transfo)
		Publishable.__init__(self, topic_pub, type_pub, rate) # 1 second message buffer

		self.rate = rate
		self.noise = np.asarray(noise)
		self.noisegen = _tools.BrownianNoise(np.diag(self.noise), 1./self.rate)
		self._time_to_measurement = rospy.get_time() + 1./self.rate
		self.static_broadcast_tf_pose()

	@staticmethod
	def from_config(**config):
		TargetCls = _tools.inheritor_from_typename(config['type'], Sensor)
		del config['type']
		return TargetCls(**config)

	def is_obs_ready(self, t):
		if self._time_to_measurement - t < 1./self.parent.parent.RATE and self.pub!=None:
			self._time_to_measurement = t + 1./self.rate + 1./self.parent.parent.RATE
			return True
		else:
			return False
	def obs(self, t):
		raise NotImplementedError('No observation can be made with the base class {}. Use a subclass or reimplement the obs function.'.format(type(self)))

class KinematicSensor(Sensor):
	def __init__(self, **kwargs):
		Sensor.__init__(self, topic_pub='/vehicle/kinematics', type_pub=Kinematics, **kwargs)

	def obs(self, t):
		obs = Kinematics()
		obs.header.frame_id = 'world'
		obs.header.stamp = rospy.Time(t)
		obs.longitudinal_accel = self.parent.acc
		obs.lateral_accel = 0
		obs.yaw_rate = self.parent.yawr
		obs.longitudinal_speed = self.parent.speed

		return obs

class PositionSensor(Sensor):
	def __init__(self, **kwargs):
		Sensor.__init__(self, topic_pub='/{}/enuposeCovs'.format(kwargs['name']),
		                type_pub=PoseWithCovarianceStamped, **kwargs)

	def obs(self, t):
		obs = PoseWithCovarianceStamped()
		obs.header.frame_id = 'world'
		obs.header.stamp = rospy.Time.from_sec(t)
		obj_local = self.get_in_frame(self.parent.parent)
		obj_local.transfo[2] = self.transfo[2] + self.parent.transfo[2]

		noise = self.noisegen.rand()
		obs.pose.pose.position.x = obj_local.transfo[0] + noise[0]
		obs.pose.pose.position.y = obj_local.transfo[1] + noise[1]
		obs.pose.pose.position.z = 0.0
		_tools.quaternion_from_yaw(obj_local.transfo[2] + noise[2], obs.pose.pose.orientation)

		obs.pose.covariance[0*6+0] = 3*self.noise[0]
		obs.pose.covariance[1*6+1] = 3*self.noise[1]
		obs.pose.covariance[5*6+5] = 3*self.noise[2]

		return obs

class GroundTruthSensor(Sensor):
	def __init__(self, **kwargs):
		Sensor.__init__(self, topic_pub='/{}/odom'.format(kwargs['name']),
		                type_pub=Odometry, **kwargs)

	def obs(self, t):
		obs = Odometry()
		obs.header.frame_id = 'world'
		obs.child_frame_id = self.frame
		obs.header.stamp = rospy.Time.from_sec(t)
		obj_local = self.get_in_frame(self.parent.parent)
		obj_local.transfo[2] = self.transfo[2] + self.parent.transfo[2]

		obs.pose.pose.position.x = obj_local.transfo[0]
		obs.pose.pose.position.y = obj_local.transfo[1]
		obs.pose.pose.position.z = 0.0
		_tools.quaternion_from_yaw(obj_local.transfo[2], obs.pose.pose.orientation)
		obs.twist.twist.linear.x = self.parent.speed
		obs.twist.twist.angular.z = self.parent.yawr

		return obs

class PerceptionSensor(Sensor, Displayable):
	def __init__(self, fov, color, perfect=False, **kwargs):
		Sensor.__init__(self, **kwargs)
		self.color = color_from_hex(color)
		self.fov = fov
		self.perfect = perfect # Will the sensor produce false observation and miss some or be perfect

	def to_marker(self, z=0):
		if not self.fov:
			raise NotImplementedError('No FOV object attached to this sensor, this might be because the base class Sensor is used.')

		marker = self.fov.to_marker(self.color, self.frame, z)
		marker.ns = self.frame
		return marker

	def _get_observables(self):
		if self.parent is not None and self.parent.parent is not None:
			parentobj, world = self.parent, self.parent.parent
			intersecting_objects = world.get_intersecting_objects(self, self.fov)
			intersecting_objects = dict(intersecting_objects)

			# Remove parent object as it is not really an observable
			if self.parent in intersecting_objects.keys():
				del intersecting_objects[self.parent]

			if not self.perfect:
				# Randomly remove observables based on distance and size
				for obj, poly in list(intersecting_objects.items()):
					dist = np.linalg.norm(np.diff((obj.transfo, self.parent.transfo), axis=0))
					dist_fact = np.min((0.2, dist/(5*self.fov.radius)))
					size_fact = np.min((0.2, 0.1/poly.area))
					if np.random.normal(0.5, 0.1+dist_fact+size_fact)>0.9:
						del intersecting_objects[obj]

				# Randomly add false observables
				from .objects import Object
				for i in range(np.random.randint(2)):
					state = {}
					state['x']    = self.parent.transfo[0] + np.random.normal(0, 50)
					state['y']    = self.parent.transfo[1] + np.random.normal(0, 50)
					state['yaw']  = np.abs(np.random.normal(0, np.sqrt(np.pi)))
					state['v']    = np.abs(np.random.normal(1, 0.05))
					state['yawr'] = np.abs(np.random.normal(0.05, 0.05))
					state['a']    = np.abs(np.random.normal(0.05, 0.01))
					shape = ['Rectangle', np.abs(np.random.normal(3, 0.5)), np.abs(np.random.normal(2, 0.5))]
					obj_config = { 'type': 'DynamicObject', 'color': '323232ff', 'shape': shape, 'state': state }

					obj = Object.from_config(name='dummy{}'.format(i), parent=self.parent.parent, **obj_config)
					intersecting_objects[obj] = None

			return intersecting_objects
		else:
			raise Exception('Sensor is not attached, parent or grandparent is None. Should be attached to an object and this object to the world')

class Camera(PerceptionSensor):
	def __init__(self, radius, openings, **kwargs):
		PerceptionSensor.__init__(self, fov=shapes.Radius(radius, openings), **kwargs)

class Mobileye(Camera):
	def __init__(self, noise=[4,2,0.5,100,100], **kwargs):
		Camera.__init__(self, topic_pub='/mobileye/can_mobileye/obstacles', type_pub=ListObstacles, noise=noise, **kwargs)
		self.pubTS = self.pubTL = None

	def obs(self, t):
		lo = ListObstacles()
		ls = ListSigns()
		ll = ListLights()
		lo.header.frame_id = ls.header.frame_id = ll.header.frame_id = self.frame
		lo.header.stamp = ls.header.stamp = ll.header.stamp = rospy.Time.from_sec(t)

		for obj, poly in PerceptionSensor._get_observables(self).items():
			obj_local = obj.get_in_frame(self)

			if isinstance(obj_local, objects.StaticObject):
				ts = self.obs_trafficsign(t, obj_local, len(ls.traffic_signs), poly)
				ls.traffic_signs.append(ts)
			elif isinstance(obj_local, objects.DynamicObject):
				o = self.obs_obstacle(t, obj_local, len(lo.obstacles), poly)
				lo.obstacles.append(o)

		return lo, ls, ll

	def obs_obstacle(self, t, obj, obj_id, poly):
		from .car import Car

		o = Obstacle()
		o.obs_timestamp_message = rospy.Time.from_sec(t)
		o.obs_age = rospy.Duration(0)
		o.obs_type = o.OBS_TYPE_CAR if isinstance(obj, Car) else o.OBS_TYPE_UNKNOWN
		o.obs_id = obj_id

		xa, ya, Oa, va, wa, aa = self.parent.x, self.parent.y, self.parent.yaw, self.parent.speed, self.parent.yawr, self.parent.acc
		xb, yb, Ob, vb, wb, ab = obj.x, obj.y, obj.yaw, obj.speed, obj.yawr, obj.acc
		ca, sa, cb, sb = np.cos(Oa), np.sin(Oa), np.cos(Ob), np.sin(Ob)
		aTb = _tools.transfo_to_matrix([self.parent.x, self.parent.y, self.parent.yaw])
		aRb = aTb[:2,:2]

		noise = self.noisegen.rand()
		noise[:2] = aRb @ noise[:2]
		o.obs_pose_x_sigma, o.obs_pose_y_sigma, o.obs_heading_sigma, o.obs_speed_x_sigma, o.obs_speed_y_sigma = self.noise

		# Rotate the velocity vector
		# 0Vb = w * R' aXb + aRb * aVb + 0Va
		accvec = wa * np.array([[-sa,ca],[-ca,-sa]]) @ [xb-xa,yb-ya]
		velvec = aRb @ [vb*cb, vb*sb] - [va*ca, va*sa] - accvec
		#v = np.linalg.norm(velvec)
		if np.all(self.noise[3:5]<100): # Noise above 100 means don't observe
			o.obs_speed_x, o.obs_speed_y = velvec+noise[3:5]


		o.obs_pose_x, o.obs_pose_y = obj.transfo[0]+noise[0], -obj.transfo[1]+noise[1] # Mobileye has a different XYZ definition from our car
		o.obs_heading = obj.transfo[2] + noise[2]

		o.obs_length = obj.shape.length
		o.obs_width = obj.shape.width
		o.obs_height = 0

		o.obs_status = o.OBS_STATUS_MOVING if obj.speed>0.01 else o.OBS_STATUS_STOPPED
		o.obs_status_confidence = 0.5
		#o.obs_confidence_level = poly.area / obs.to_polygon().to_shapely().area

		o.obs_road_lane = o.OBS_ROAD_LANE_INVALID

		return o

	def obs_trafficsign(self, t, obj, obj_id, poly):
		ts = TrafficSign()

		ts.tsr_timestamp_message = rospy.Time.from_sec(t)
		ts.tsr_age = rospy.Duration(0)
		ts.tsr_id = obj_id
		ts.tsr_pose_x, ts.tsr_pose_y = obj.transfo[0], -obj.transfo[1] # Mobileye has a different XYZ definition from our car
		ts.tsr_type = ts.TSR_TYPE_INVALID
		ts.tsr_subtype = ts.TSR_SUBTYPE_INVALID
		ts.tsr_pose_x_sigma, ts.tsr_pose_y_sigma = 4, 2

		return ts

	# Override publishable functions to allow for two publisher
	def toggle_publication(self, prefix, topic=None, type=None):
		topicTS = '/mobileye/can_mobileye/traffic_signs'
		typeTS = ListSigns
		topicTL = '/mobileye/can_mobileye/traffic_lights'
		typeTL = ListLights

		if Publishable.toggle_publication(self, prefix, topic, type):
			if prefix:
				topicTS = self.parent.frame+topicTS
				topicTL = self.parent.frame+topicTL
			self.pubTS = rospy.Publisher(topicTS, typeTS, queue_size=self.queue_size)
			self.pubTL = rospy.Publisher(topicTL, typeTL, queue_size=self.queue_size)
			rospy.loginfo('Started publishing {} on {}'.format(typeTS.__name__, topicTS))
			rospy.loginfo('Started publishing {} on {}'.format(typeTL.__name__, topicTL))
			return True
		else:
			del self.pubTS
			del self.pubTL
			self.pubTS = None
			self.pubTL = None
			rospy.loginfo('Stopped publishing on {}'.format(topicTS))
			rospy.loginfo('Stopped publishing on {}'.format(topicTL))
			return False

	def publish(self, data):
		Publishable.publish(self, data[0])
		if self.pubTS is not None:
			try:
				self.pubTS.publish(data[1])
			except rospy.exceptions.ROSException as e:
				rospy.loginfo(e)
		if self.pubTL is not None:
			try:
				self.pubTL.publish(data[2])
			except rospy.exceptions.ROSException as e:
				rospy.loginfo(e)

class Lidar(PerceptionSensor):
	def __init__(self, radius, **kwargs):
		PerceptionSensor.__init__(self, fov=shapes.Circle(radius), topic_pub='/lidar/polygons', type_pub=Polygons, **kwargs)

	def obs(self, t):
		polygons = Polygons()
		polygons.header.stamp = rospy.Time.from_sec(t)
		polygons.header.frame_id = self.frame

		for obj, poly in PerceptionSensor._get_observables(self).items():
			obj_local = obj.get_in_frame(self)

			obj_to_parent_mat = _tools.transfo_to_matrix(obj_local.transfo)
			shape_in_parent = obj.shape.to_polygon().transform(obj_to_parent_mat, True)

			p = Polygon()
			for x,y in shape_in_parent.vertices:
				p.points.append(Point32(x, y, 0))
			polygons.polygons.append(p)

		return polygons
