#!/usr/bin/env python

import numpy as np
from .objects import DynamicObject

class Car(DynamicObject):
	def __init__(self, **kwargs):
		DynamicObject.__init__(self, **kwargs)

	def evo(self, dt):
		fx = np.array([[
			 self.speed*np.cos(self.yaw)*np.cos(self.yawr),
			 self.speed*np.sin(self.yaw)*np.cos(self.yawr),
			 (self.speed*np.sin(self.yawr))/3,
			 self.acc,
			 self.yawr_cmd,
			 self.acc_cmd
		]]).T

		self.state += dt*fx
