#!/usr/bin/env python

import rospy
import time
import threading

import numpy as np
import shapely.geometry
import matplotlib.pyplot as plt

from geometry_msgs.msg import Point
from rosgraph_msgs.msg import Clock
from map_server.srv import MapRequest
from visualization_msgs.msg import Marker, MarkerArray
from map_server_msgs.msg import MapTile, MapRequestInput

from . import shapes
from ._ros_bridge import get_clean_marker
from ._tools import transfo_to_matrix
from .objects import Object, DynamicObject, StaticObject, TrafficSign
from .car import Car
from simulation.srv import Listen, ListenRequest

class World(object):
	RATE  = 1000.

	def __init__(self, config):
		self.objects = []
		self.threads = []
		self.markers_pub = rospy.Publisher('simulation_markers', MarkerArray, queue_size=5)
		self.map_pub = rospy.Publisher('/maptile', MapTile, queue_size=1)
		rospy.Service('listen', Listen, self.handle_listen_service)
		self.frame = ''
		self.plot = False

		# Load the map
		self.map = World.load_map(**(config['map']))

		# Temporarly store listen request from config
		listen_requests = []

		# Load dynamic objects from config
		for obj_name, obj_config in config['objects'].items():
			if 'publish' in obj_config.keys():
				listen_requests.append(obj_name)
				del obj_config['publish']

			obj = Object.from_config(name=obj_name, parent=self, **obj_config)
			self.objects.append(obj)

		# Load static objects from map
		if self.map:
			for rs in self.map.road_signals:
				if rs.type!=rs.TYPE_BOLLARD:
					rs_state = { 'x': rs.position.point.x, 'y': rs.position.point.y, 'yaw': 0, 'v': 0, 'yawr': 0, 'a': 0 }
					obj_config = { 'color': '323232ff', 'shape': [ 'Circle', 0.5 ], 'state': rs_state }
					obj_name = 'rs{}'.format(rs.id)

					if rs.id in config['rsus'].keys():
						obj_config['sensors'] = config['rsus'][rs.id]['sensors']
						obj_config['comm_range'] = config['rsus'][rs.id]['comm_range']

						if 'publish' in config['rsus'][rs.id].keys():
							listen_requests.append(obj_name)
							del config['rsus'][rs.id]['publish']

					obj = TrafficSign(name=obj_name, parent=self, **obj_config)
					self.objects.append(obj)

		# Start simulation threads for active (moving/perceiving) objects
		for obj in self.objects:
			if len(obj.sensors)>0 or isinstance(obj, DynamicObject):
				self.threads.append(threading.Thread(target=Object.sim, args=(obj,)))

		# Start the clock server
		self.sim_speed = 1.
		self.sim_pause = False
		self.sim_time = 0
		self.clock_pub = rospy.Publisher('/clock', Clock, queue_size=World.RATE)
		self.clock_server_thread = threading.Thread(target=self.clock_server)
		self.clock_server_thread.start()

		# Start object threads (observation & evolution)
		for t in self.threads:
			t.start()

		# Call listen request from config
		for obj_name in listen_requests:
			self.handle_listen_service(ListenRequest(obj_name, True))

	def publish_markers(self):
		markers = MarkerArray()
		markers.markers.append(get_clean_marker())

		for obj in self.objects:
			if self.plot:
				self.sim_pause = True
				plt.plot(0,0, 'x', color='black')
				plt.plot(obj.x,obj.y, 'x', color='black')
				plt.plot([obj.x,obj.x+np.cos(obj.yaw)], [obj.y,obj.y+np.sin(obj.yaw)], color='red')

			if not (isinstance(obj, TrafficSign) and len(obj.sensors)==0):
				markers.markers.extend(obj.to_marker())
			if isinstance(obj, StaticObject):
				obj.static_broadcast_tf_pose()

		if self.plot:
			self.plot = False
			plt.axis('equal')
			plt.show()

		self.markers_pub.publish(markers)
		self.map_pub.publish(self.map)

	def get_intersecting_objects(self, target, fov):
		intersecting_objs = []

		# Project the sensor fov to the working frame
		world_to_target_mat = target.get_matrix_to_world()
		target_to_world_mat = np.linalg.inv(world_to_target_mat)
		fov_in_world = fov.to_polygon().transform(world_to_target_mat).to_shapely()

		for obj in self.objects:
			# Get object polygons (in the working frame as well)
			world_to_obj_mat = obj.get_matrix_to_world()
			obj_in_world = obj.shape.to_polygon().transform(world_to_obj_mat).to_shapely()

			# Use shapely to compute the intersection
			if fov_in_world.intersects(obj_in_world):
				intersection = fov_in_world.intersection(obj_in_world)

				# Get the intersection polygon back in the sensor frame
				intersection_in_target = shapes.Polygon.from_shapely(intersection).transform(target_to_world_mat)
				intersecting_objs.append((obj, intersection_in_target))

		return intersecting_objs

	@staticmethod
	def load_map(ref, center=(0,0,0), radius=0):
		req = MapRequestInput()
		req.frame_id = 'world'
		req.srid = 0
		req.ref_coord_system = Point(*ref)
		req.position = Point(*center)
		req.radius = radius
		req.links_requested = True
		req.link_borders_requested = True
		req.road_signals_requested = True

		try:
			rospy.wait_for_service('/maptile_request', timeout=5)
			mapreq_client = rospy.ServiceProxy('/maptile_request', MapRequest)
			mapreq = mapreq_client.call(req)
			mapreq_client.close()
			return mapreq.out

		except rospy.exceptions.ROSException as e:
			rospy.logwarn('Waited for map_server service: map_server may be disabled ({})'.format(e))
		except rospy.ServiceException as e:
			rospy.logwarn('Service call failed: {}'.format(e))

	def handle_listen_service(self, request):
		objs = dict([(obj.frame,obj) for obj in self.objects])
		request_response = False
		if request.name in objs.keys():
			if objs[request.name].comm is not None:
				objs[request.name].comm.toggle_publication(request.prefix)
			for sensor in objs[request.name].sensors:
				request_response = sensor.toggle_publication(request.prefix)
		else:
			rospy.logwarn('Unknown object name "{}" in {} trying to toggle listening'.format(request.name, list(objs.keys())))

		return request_response

	def clock_server(self):
		while not rospy.is_shutdown():
			if not self.sim_pause:
				self.sim_time += 1./World.RATE

			try:
				self.clock_pub.publish(rospy.Time(self.sim_time))
			except rospy.exceptions.ROSException:
				pass

			time.sleep(1./(World.RATE*self.sim_speed))

	def get_matrix_to_world(self):
		return transfo_to_matrix([0, 0, 0])
