#!/usr/bin/env python

import numpy as np

import rospy
from . import shapes
from ._ros_bridge import Publishable
from multiception.msg import Bytes

class Communication(Publishable):
	'''
	Communication simulation. Listens to /com/out and publishes on /com/in.
	When a message is received, the originating vehicle is determined and the
	message is transmitted to in-range receivers.
	'''
	def __init__(self, parent, range):
		Publishable.__init__(self, '/com/in', Bytes, 10)
		self.sub = None
		self.parent = parent
		self.range = shapes.Circle(range)

	def toggle_publication(self, prefix, topic=None, type=None):
		toggled = Publishable.toggle_publication(self, prefix, topic, type)
		if toggled:
			topic_out = (self.parent.frame if prefix else '') + '/com/out'
			self.sub = rospy.Subscriber(topic_out, self.type, self.send, queue_size=self.queue_size)
		else:
			del self.sub
			self.sub = None

		return toggled

	def send(self, msg):
		for obj in self.parent.parent.objects:
			if obj.comm!=None and obj.comm!=self and np.linalg.norm(self.parent.transfo[:2]-obj.transfo[:2])<self.range.radius:
				obj.comm.receive(msg)

	def receive(self, msg):
		# Wait a random amount of time in the background then publish the received message
		rospy.Timer(rospy.Duration(np.random.normal(0.15, 0.05)), lambda event: self.publish(msg), oneshot=True)
