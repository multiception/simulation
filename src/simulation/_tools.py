#!/usr/bin/env python

import numpy as np
import rospy
import tf_conversions
np.set_printoptions(precision=2, formatter={'float': '{:0.2f}'.format})

def get_ns(as_tf=False):
	ns = rospy.get_namespace()
	if as_tf:
		ns = '/'.join([subns for subns in ns.split('/') if subns])
	return ns

def transfo_to_matrix(transfo):
	return np.array([
		[np.cos(transfo[2]), -np.sin(transfo[2]), transfo[0]],
		[np.sin(transfo[2]),  np.cos(transfo[2]), transfo[1]],
		[0, 0, 1]])

def get_inheritors(cls):
	inheritors = []

	for subcls in cls.__subclasses__():
		inheritors.append(subcls)
		inheritors.extend(get_inheritors(subcls))

	return inheritors

def inheritor_from_typename(typename, cls):
	inheritors = dict([(inheritor.__name__, inheritor) for inheritor in get_inheritors(cls)])
	return inheritors[typename]

def quaternion_from_yaw(yaw, target):
	q = tf_conversions.transformations.quaternion_from_euler(0, 0, yaw)
	target.x = q[0]
	target.y = q[1]
	target.z = q[2]
	target.w = q[3]
	return target

def yaw_from_quaternion(q):
	return tf_conversions.transformations.euler_from_quaternion([q.x, q.y, q.z, q.w])[2]

def array_from_points(points):
		arr = np.zeros((2, len(points)), dtype=np.float32)
		for i, p in enumerate(points):
			arr[:, i] = p.x, p.y
		return arr

def rotate_cov_2D(theta, cov, cov_indep=None):
	rotmat = transfo_to_matrix((0, 0, theta))[:2,:2]
	cov[:2,:2] = rotmat @ cov[:2,:2] @ rotmat.T
	if cov_indep is not None:
		cov_indep[:2,:2] = rotmat @ cov_indep[:2,:2] @ rotmat.T

class BrownianNoise(object):
	def __init__(self, cov, dt):
		self.cov = np.asarray(cov)
		self.previous = np.zeros(len(self.cov))
		self.dt = dt

	def rand(self, dt=None):
		if dt is None:
			dt = self.dt

		p = self.previous[:]
		cov = self.cov/20 + (np.random.uniform(0, 100)<5)*self.cov/2 # Sporadically give a large noise
		r = np.random.multivariate_normal(-self.previous/20, cov*dt)
		self.previous = self.previous + r
		return self.previous
