#!/usr/bin/env python

import numpy as np

import rospy
import yaml
from . import _tools
from .communication import Communication
from .shapes import Shape
from .sensors import Sensor, PerceptionSensor
from ._ros_bridge import Displayable

class Object(Displayable):
	DEFAULT_STATE = { 'x': 0, 'y': 0, 'yaw': 0, 'v': 0, 'yawr': 0, 'a': 0 }
	DEFAULT_SHAPE = [ 'Rectangle', 4, 2 ]

	def __init__(self, shape, state=DEFAULT_STATE, sensors={}, comm_range=0, **kwargs):
		'''
		state (Nx1) = { x, y, yaw (heading), speed, yaw rate (wheel angle), acceleration }
		command = { yaw rate, acceleration }
		'''
		Displayable.__init__(self, transfo=np.array([state['x'], state['y'], state['yaw']]), **kwargs)

		self.state = np.zeros((6,1), dtype=np.float32)
		self.covar = np.zeros((6,6), dtype=np.float32)
		self.shape = Shape.from_config(shape)
		self.state[:,0] = [state[i] for i in ['x', 'y', 'yaw', 'v', 'yawr', 'a']]

		# Saving sensor config to be used as static configuration files by multiception
		if len(sensors)>0:
			with open('/tmp/config_{}.yaml'.format(self.frame), 'w') as cfg:
				sim_cfg = { 'type': type(self).__name__, 'sensors': sensors}
				if isinstance(self, StaticObject):
					sim_cfg['pose'] = [self.x, self.y, self.yaw]
				yaml.dump(sim_cfg, cfg)

		self.comm = Communication(self, comm_range) if comm_range>1 else None
		self.observations = []
		self.sensors = []
		for sensor_name, sensor_config in sensors.items():
			transfo = [sensor_config['dx'], sensor_config['dy'], sensor_config['dyaw']]
			del sensor_config['dx'], sensor_config['dy'], sensor_config['dyaw']
			self.sensors.append(Sensor.from_config(parent=self, name=sensor_name, transfo=transfo, **sensor_config))

	@property
	def x(self): return float(self.state[0,0])
	@property
	def y(self): return float(self.state[1,0])
	@property
	def yaw(self): return float(self.state[2,0])
	@property
	def speed(self): return float(self.state[3,0])
	@property
	def yawr(self): return float(self.state[4,0])
	@property
	def acc(self): return float(self.state[5,0])
	@x.setter
	def x(self, val): self.state[0,0] = val
	@y.setter
	def y(self, val): self.state[1,0] = val
	@yaw.setter
	def yaw(self, val): self.state[2,0] = val
	@speed.setter
	def speed(self, val): self.state[3,0] = val
	@yawr.setter
	def yawr(self, val): self.state[4,0] = val
	@acc.setter
	def acc(self, val): self.state[5,0] = val

	@staticmethod
	def from_config(**config):
		TargetCls = _tools.inheritor_from_typename(config['type'], Object)
		del config['type']
		return TargetCls(**config)

	def obs(self, t):
		for sensor in self.sensors:
			if Sensor.is_obs_ready(sensor, t):
				sensor.publish(sensor.obs(t))

	def sim(self):
		# Called in world.py in threads, thus calling the correct object's function
		rate = rospy.Rate(self.parent.RATE)
		rate.sleep()
		prevt = rospy.get_time()
		while not rospy.is_shutdown():
			try:
				# Prevents ROS from returning here before the clock server is effectively up-to-date
				rospy.sleep(0.0001)
			except rospy.exceptions.ROSInterruptException as e:
				rospy.loginfo(e)

			t = rospy.get_time()
			self.sim(t, t-prevt)
			prevt = t

			try:
				rate.sleep()
			except rospy.exceptions.ROSInterruptException as e:
				rospy.loginfo(e)

	def to_marker(self, shape=True, comrange=True, sensors=True):
		'''
		Limited as long as markerarray and trianglelist dont support transparency.
		For the moment, sensors are displayed deepest, car shalowest and comrange in between.
		'''

		if not (shape | comrange | sensors):
			# ToDo: Change exception for something more specific
			raise Exception('At least one in { shape, comrange, sensors } must me true')

		markers = []

		if shape:
			shape_marker = self.shape.to_marker(color=self.color, z=-1)
			shape_marker.header.frame_id = self.frame
			shape_marker.ns = self.frame
			markers.append(shape_marker)

		if sensors:
			for sensor in self.sensors:
				sensor.static_broadcast_tf_pose()
				if isinstance(sensor, PerceptionSensor):
					markers.append(sensor.to_marker(z=-10))

		if comrange:
			pass

		return markers

class StaticObject(Object):
	def __init__(self, **kwargs):
		Object.__init__(self, **kwargs)

	def sim(self, t, dt):
		self.obs(t)

class TrafficSign(StaticObject):
	def __init__(self, **kwargs):
		StaticObject.__init__(self, **kwargs)

class DynamicObject(Object):
	def __init__(self, **kwargs):
		Object.__init__(self, **kwargs)
		self.command = np.zeros((2,1), dtype=np.float32)
		self.MIN_YAWR, self.MAX_YAWR = -np.pi, np.pi
		self.MIN_ACC, self.MAX_ACC = -5, 10

	def evo(self, dt):
		raise NotImplementedError('No evo method implemented for {}. This might be because the base class DynamicObject is used.'.format(self))

	def sim(self, t, dt):
		self.obs(t)
		self.evo(dt)
		#self.yaw = np.fmod(self.yaw, 2*np.pi)
		self.transfo = self.state[:3,0]
		self.broadcast_tf_pose()
		#self.covar += Galpha

	@property
	def yawr_cmd(self): return self.command[0,0]
	@property
	def acc_cmd(self): return self.command[1,0]
	
	@yawr_cmd.setter
	def yawr_cmd(self, val): self.command[0,0] = np.max(self.MIN_YAWR, np.min(self.MAX_YAWR, val))
	@acc_cmd.setter
	def acc_cmd(self, val): self.command[1,0] = np.max(self.MIN_ACC, np.min(self.MAX_ACC, val))
