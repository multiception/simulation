#!/usr/bin/env python

import rospy
import numpy as np
import shapely.geometry

from . import _tools
from . import _ros_bridge

class Shape(object):
	def __init__(self):
		pass

	def to_polygon(self):
		raise NotImplementedError('Base class used, please use a subclass or reimplement the to_polygon function.')

	def to_marker(self, color, frame="", z=0):
		poly = self.to_polygon()
		return _ros_bridge.polygon_to_marker(poly.vertices, color=color, frame=frame, z=z)

	@staticmethod
	def from_config(config):
		'''
		Shape config is a 2-tuple in the form (Typename, (args,...))
		''' 
		TargetCls = _tools.inheritor_from_typename(config[0], Shape)
		return TargetCls(*(config[1:]))

class Polygon(Shape):
	def __init__(self, vertices):
		Shape.__init__(self)
		self.vertices = vertices

	def to_polygon(self):
		return self

	def transform(self, transfo_mat, copy=False):
		'''
		transfo_mat: 3x3 is an optional 3-tuple of the form (dx, dy, dtheta)
		'''

		vert = np.vstack(( self.vertices.T, np.ones(self.vertices.shape[0]) ))
		new_vert = transfo_mat.dot(vert).T[:, :2]

		if copy:
			return Polygon(new_vert)
		else:
			self.vertices = new_vert
			return self

	def to_shapely(self):
		return shapely.geometry.Polygon(self.vertices)

	@property
	def area(self):
		return self.to_shapely().area

	@staticmethod
	def from_shapely(shapely_polygon):
		if isinstance(shapely_polygon, shapely.geometry.MultiPolygon):
			#return [Polygon.from_shapely(p) for p in shapely_polygon]
			return Polygon.from_shapely(shapely_polygon[0])
		elif isinstance(shapely_polygon, shapely.geometry.Polygon):
			return Polygon(np.array([coor for coor in shapely_polygon.exterior.coords]))
		elif isinstance(shapely_polygon, shapely.geometry.Point):
			return Polygon(np.array(shapely_polygon.coords))
		else:
			raise Exception('Unsupported type to convert to Polygon "{}"'.format(type(shapely_polygon)))

class Circle(Shape):
	RAD_STEP = np.pi/20

	def __init__(self, radius):
		Shape.__init__(self)
		self.radius = radius
		self.openings = (0, 2*np.pi)

	def to_polygon(self):
		circle = np.arange(self.openings[0], self.openings[1], Circle.RAD_STEP)
		points = self.radius*np.array([np.cos(circle), np.sin(circle)]).T
		return Polygon(np.vstack(( points, points[0] )))

	@property
	def length(self): return self.radius
	@property
	def width(self): return self.radius

class Radius(Circle):
	def __init__(self, radius, openings):
		Circle.__init__(self, radius)
		self.openings = openings

	def to_polygon(self):
		circle_vertices = Circle.to_polygon(self).vertices
		return Polygon(np.vstack(( np.zeros(2), circle_vertices[:-1], np.zeros(2) )))

class Rectangle(Shape):
	def __init__(self, length, width):
		Shape.__init__(self)
		self.length = length
		self.width = width

	def to_polygon(self):
		corners = np.array([
			[ self.length/2,  self.width/2],
			[ self.length/2, -self.width/2],
			[-self.length/2, -self.width/2],
			[-self.length/2,  self.width/2],
			[ self.length/2,  self.width/2]])
		return Polygon(corners)

class Ellipsis(Rectangle):
	pass
