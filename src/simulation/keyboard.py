#!/usr/bin/env python

import tty
import sys
import termios

class Keyboard:
	KEY_UP = 65
	KEY_DOWN = 66
	KEY_LEFT = 67
	KEY_RIGHT = 68
	KEY_SPACE = 32
	KEY_RETURN = 10
	KEY_ESCAPE = 27
	CTRLSEQ_ARR1 = 27
	CTRLSEQ_ARR2 = 91

	def __init__(self, target=sys.stdin):
		self.target = target
		self.orig_settings = None

	def __enter__(self):
		self.orig_settings = termios.tcgetattr(self.target)
		tty.setcbreak(self.target)
		return self

	def __exit__(self, type, value, traceback):
		termios.tcsetattr(self.target, termios.TCSADRAIN, self.orig_settings)

	def read(self):
		return ord(self.target.read(1)[0])
