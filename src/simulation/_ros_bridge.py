#!/usr/bin/env python

import numpy as np
from copy import copy as shallowcopy

import rospy
import tf2_ros

from . import _tools
from visualization_msgs.msg import Marker
from std_msgs.msg import ColorRGBA
from geometry_msgs.msg import Point, TransformStamped, Quaternion

def get_ns():
	return rospy.get_namespace()
'''
class PoseEmitter:
	def __init__(self, topic, frame_from, frame_to):
		self.br_ = tf.TransformBroadcaster()
		self.pose_pub_ = rospy.Publisher(topic, PoseWithCovarianceStamped, queue_size=5)

		self.ns = get_ns()
		self.frame_from_ = frame_from
		self.frame_to_ = frame_to if self.ns else self.ns+'/'+frame_to

	def state_to_pose(self, state_holder):
		msg = PoseWithCovarianceStamped()
		pos, orient = msg.pose.pose.position, msg.pose.pose.orientation
		
		msg.header.frame_id = self.frame_to_
		msg.header.stamp = rospy.get_rostime()

		pos.x, pos.y, pos.z = state_holder.x, state_holder.y, 0
		orient.x, orient.y, orient.z, orient.w = quaternion_from_euler(0, 0, state_holder.yaw)

		C = np.zeros((6,6), dtype=np.float64); C[:2,:2] = state_holder.covar[:2,:2]; #cov[3:4,3:4] = C[3,3]
		msg.pose.covariance = C.flatten()

		return msg

	def emit(self, state_holder):
		pose = self.state_to_pose(state_holder)
		self.pose_pub_.publish(pose)
		self.broadcast_tf_pose(pose)
'''

def polygon_to_marker(polygon, color=ColorRGBA(0,0,0,0), frame="", z=0):
	marker = Marker()
	marker.header.stamp = rospy.get_rostime()
	marker.header.frame_id = frame
	marker.type = Marker.TRIANGLE_LIST
	marker.action = Marker.ADD
	marker.frame_locked = True
	marker.color = color
	marker.scale.x = marker.scale.y = marker.scale.z = 1
	marker.pose.position = Point(0, 0, 0)
	marker.pose.orientation = Quaternion(0, 0, 0, 1)
	marker.points = [None]*(3*len(polygon))
	marker.colors = [color]*(len(polygon))

	centroid = np.mean(polygon, axis=0)
	for i in range(len(polygon)-1):
		marker.points[3*i+0] = Point(centroid[0], centroid[1], z)
		marker.points[3*i+1] = Point(polygon[i,0], polygon[i,1], z)
		marker.points[3*i+2] = Point(polygon[i+1,0], polygon[i+1,1], z)

	marker.points[-3] = Point(centroid[0], centroid[1], 0)
	marker.points[-2] = Point(polygon[-1,0], polygon[-1,1], 0)
	marker.points[-1] = Point(polygon[0,0], polygon[0,1], 0)

	return marker

def color_from_hex(hexcol):
	return ColorRGBA(*[int(hexcol[i:i+2],16)/255.0 for i in (0,2,4,6)])

def get_clean_marker(namespace=''):
	marker = Marker()
	marker.header.stamp = rospy.get_rostime()
	marker.header.frame_id = 'world'
	marker.action = Marker.DELETEALL
	marker.ns = namespace
	return marker

class Publishable(object):
	def __init__(self, topic_pub, type_pub, queue_size):
		self.pub = None
		self.topic, self.type, self.queue_size = topic_pub, type_pub, queue_size

	def toggle_publication(self, prefix, topic=None, type=None):
		if topic is not None: self.topic = topic
		if type is not None: self.type = type

		if self.pub is None:
			if prefix: self.topic = self.parent.frame+self.topic
			self.pub = rospy.Publisher(self.topic, self.type, queue_size=self.queue_size)
			rospy.loginfo('Started publishing {} on {}'.format(self.type.__name__, self.topic))
			return True
		else:
			del self.pub
			self.pub = None
			rospy.loginfo('Stopped publishing on {}'.format(self.topic))
			return False

	def publish(self, data):
		if self.pub is not None:
			try:
				self.pub.publish(data)
			except rospy.exceptions.ROSException as e:
				rospy.loginfo(e)

class Transformable(object):
	tfBuffer = None
	tfListener = None

	def __init__(self, parent, name, transfo):
		self.parent = parent
		self.frame = (parent.frame + '/' if parent.frame else '') + name
		self.transfo = transfo
		self.br_ = tf2_ros.TransformBroadcaster()
		self.sbr_ = tf2_ros.StaticTransformBroadcaster()
		if Transformable.tfBuffer is None:
			Transformable.tfBuffer = tf2_ros.Buffer()
			Transformable.tfListener = tf2_ros.TransformListener(Transformable.tfBuffer)

	def to_Transform(self):
		t = TransformStamped()

		t.header.stamp = rospy.get_rostime()
		t.header.frame_id = self.parent.frame if len(self.parent.frame)>1 else 'world'
		t.child_frame_id = self.frame
		t.transform.translation.x = self.transfo[0]
		t.transform.translation.y = self.transfo[1]
		t.transform.translation.z = 0.0
		_tools.quaternion_from_yaw(self.transfo[2], t.transform.rotation)

		return t

	def broadcast_tf_pose(self):
		t = self.to_Transform()
		try:
			self.br_.sendTransform(t)
		except rospy.exceptions.ROSException as e:
			rospy.loginfo(e)

	def static_broadcast_tf_pose(self):
		t = self.to_Transform()
		self.sbr_.sendTransform(t)

	def get_in_frame_tf(self, frame):
		'''
		Deprecated, `get_in_frame` is prefered.
		'''
		copy = shallowcopy(self)
		try:
			t = Transformable.tfBuffer.lookup_transform(frame, self.frame, rospy.Time())
		except (tf2_ros.LookupException, tf2_ros.ConnectivityException, tf2_ros.ExtrapolationException):
			return None

		yaw = _tools.yaw_from_quaternion(t.transform.rotation)
		copy.transfo = [t.transform.translation.x, t.transform.translation.y, yaw]
		copy.frame = frame
		return copy

	def get_in_frame(self, target):
		copy = shallowcopy(self)
		self_to_world_mat = self.get_matrix_to_world()
		world_to_target_mat = np.linalg.pinv(target.get_matrix_to_world())
		self_to_target_mat = world_to_target_mat.dot(self_to_world_mat)

		copy.transfo = [self_to_target_mat[0,2], self_to_target_mat[1,2], np.arctan2(-self_to_target_mat[0,1], self_to_target_mat[0,0])]
		copy.frame = target.frame
		return copy

	def get_matrix_to_world(self):
		mat = _tools.transfo_to_matrix(self.transfo)
		return self.parent.get_matrix_to_world().dot(mat)

class Displayable(Transformable):
	def __init__(self, parent, name, transfo, color):
		Transformable.__init__(self, parent, name, transfo)
		self.color = color_from_hex(color)

	def to_marker(self):
		raise NotImplementedError('No to_marker function defined for base class Displayable. Inherit from it and redefine the to_marker function.')
